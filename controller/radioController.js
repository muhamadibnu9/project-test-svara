const Radio = require('../models/Radio.js');

exports.create = async (req, res) => {
    const { _id, name, city, frequency, logo, stream, website } = req.body;

    try {
        const radio = new Radio({ _id, name, city, frequency, logo, stream, website });
        await radio.save();

        res.send('You made a Radio')
    } catch (err) {
        return res.status(422).send(err.message);
    }
    
};

// Retrieve and return all Radio from the database.
exports.findAll = (req, res) => {
    Radio.find()
    .then(radio => {
        res.send(radio);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving radio."
        });
    });
};

// Find a single radio with a radioId
exports.findOne = (req, res) => {
    Radio.findById(req.params.radioId)
    .then(radio => {
        if(!radio) {
            return res.status(404).send({
                message: "Radio not found with id " + req.params.radioId
            });            
        }
        res.send(radio);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Radio not found with id " + req.params.radioId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Radio with id " + req.params.radioId
        });
    });
};

// Update a radio identified by the radioId in the request
exports.update = (req, res) => {
    // Validate Request
    if(!req.body) {
        return res.status(400).send({
            message: "radio content can not be empty"
        });
    }

    // Find radio and update it with the request body
    Radio.findByIdAndUpdate(req.params.radioId, req.body, { useFindAndModify: false })
    .then(radio => {
        if(!radio) {
            return res.status(404).send({
                message: "radio not found with id " + req.params.radioId
            });
        }else res.send('You Update a Radio');
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "radio not found with id " + req.params.radioId
            });                
        }
        return res.status(500).send({
            message: "Error updating radio with id " + req.params.radioId
        });
    });
};

// Delete a radio with the specified radioId in the request
exports.delete = (req, res) => {
    Radio.findByIdAndRemove(req.params.radioId)
    .then(radio => {
        if(!radio) {
            return res.status(404).send({
                message: "radio not found with id " + req.params.radioId
            });
        }
        res.send({message: "radio deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "radio not found with id " + req.params.radioId
            });                
        }
        return res.status(500).send({
            message: "Could not delete radio with id " + req.params.radioId
        });
    });
};