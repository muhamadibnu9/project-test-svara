module.exports = (app) => {
    const user = require('../controller/userController.js');

    //Register User
    app.post('/register', user.register);

    //Login User
    app.post('/login', user.login);

    //Guest-Mode
    app.post('/guest', user.guest);
}
