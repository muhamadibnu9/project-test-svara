module.exports = (app) => {
    const radio = require('../controller/radioController.js');

    // Retrieve all Radio
    app.get('/radio', radio.findAll);

    // Retrieve a single Radio with noteId
    app.get('/radio/:radioId', radio.findOne);

    // Create a new Radio
    app.post('/radio', radio.create);

    // Update a Radio with radioId
    app.put('/radio/:radioId', radio.update);

    // Delete a Radio with radioId
    app.delete('/radio/:radioId', radio.delete);
}