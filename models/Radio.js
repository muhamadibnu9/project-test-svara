const mongoose = require('mongoose');

const RadioSchema = new mongoose.Schema({
    _id: {
        type: Number,
        unique:true,
        required:true
    },
    name: {
        type : String,
        required : true,
    },
    city: {
        type: String,
        required:  true
    },
    frequency: {
        type: String,
        required:  true
    },
    logo: {
        type: String
    },
    stream: {
        type: String
    },
    website: {
        type: String
    }

});

module.exports = mongoose.model('Radio', RadioSchema);