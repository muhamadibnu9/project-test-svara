require('dotenv').config();

//require('./models/User');
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const requireAuth = require('./middlewares/requireAuth');

const app = express();

app.use(bodyParser.json());
//app.use(userRoutes);

const mongoUri = process.env.URI_MONGODB;
mongoose.Promise = global.Promise;

mongoose.connect(mongoUri,{
    useNewUrlParser:true,
    useCreateIndex:true,
    useUnifiedTopology: true
});

mongoose.connection.on('connected',()=>{
    console.log('berhasil');
});

mongoose.connection.on('error',err => {
    console.log('Error : '+err);
    process.exit();
});

app.get('/', requireAuth, (req, res) => {
    res.send(`Your Email ${req.user.email}`);
});


require('./routes/radioRoutes.js')(app);
require('./routes/userRoutes.js')(app);
var port = process.env.PORT || 8888;
app.listen(port,()=>{
    console.log('listening port '+port)
});